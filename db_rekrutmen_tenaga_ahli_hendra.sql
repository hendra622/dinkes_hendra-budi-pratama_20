/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100128
 Source Host           : localhost:3306
 Source Schema         : db_rekrutmen_tenaga_ahli_hendra

 Target Server Type    : MySQL
 Target Server Version : 100128
 File Encoding         : 65001

 Date: 18/06/2021 11:06:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for m_kecamatan
-- ----------------------------
DROP TABLE IF EXISTS `m_kecamatan`;
CREATE TABLE `m_kecamatan`  (
  `id_kecamatan` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nama_kecamatan` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id_kota` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_kecamatan`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_kecamatan
-- ----------------------------
INSERT INTO `m_kecamatan` VALUES ('317105', 'Kemayoran', '3171');
INSERT INTO `m_kecamatan` VALUES ('317203', 'Pasar Rebo', '3172');
INSERT INTO `m_kecamatan` VALUES ('317401', 'Setiabudi', '3174');

-- ----------------------------
-- Table structure for m_kota
-- ----------------------------
DROP TABLE IF EXISTS `m_kota`;
CREATE TABLE `m_kota`  (
  `id_kota` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nama_kota` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id_provinsi` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_kota`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_kota
-- ----------------------------
INSERT INTO `m_kota` VALUES ('3171', 'Jakarta Pusat', '31');
INSERT INTO `m_kota` VALUES ('3172', 'Jakarta Timur', '31');
INSERT INTO `m_kota` VALUES ('3174', 'Jakarta Selatan', '31');

-- ----------------------------
-- Table structure for m_provinsi
-- ----------------------------
DROP TABLE IF EXISTS `m_provinsi`;
CREATE TABLE `m_provinsi`  (
  `id_provinsi` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nama_provinsi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_provinsi
-- ----------------------------
INSERT INTO `m_provinsi` VALUES ('31', 'DKI Jakarta');

-- ----------------------------
-- Table structure for m_puskesmas
-- ----------------------------
DROP TABLE IF EXISTS `m_puskesmas`;
CREATE TABLE `m_puskesmas`  (
  `id_puskesmas` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nama_puskesmas` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `alamat_puskesmas` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `no_telp_puskesmas` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `id_kecamatan` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  PRIMARY KEY (`id_puskesmas`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of m_puskesmas
-- ----------------------------
INSERT INTO `m_puskesmas` VALUES ('P31710002', 'Puskesmas Kecamatan Kemayoran', 'kp Sukasari no 13', '021-002002', '317105');
INSERT INTO `m_puskesmas` VALUES ('P31720003', 'Puskesmas Kecamatan Pasar Rebo', 'Jl Kalisari no 10', '021-003003', '317203');

SET FOREIGN_KEY_CHECKS = 1;
