
Tugas Hands On Hendra Budi Pratama No. Urut 20

- API ini dibuat dengan menggunakan bahasa pemrograman PHP dan Framework CodeIgniter 3
- Menggunakan library CodeIgniter Rest Server

Panduan Clone dan Deploy API Rekrutmen Tenaga Ahli :
1. Buat database dengan nama db_rekrutmen_tenaga_ahli_hendra
2. Import database db_rekrutmen_tenaga_ahli_hendra.sql pada database yang sudah dibuat
3. buka file application/config/database.php
4. sesuaikan data berikut dengan environment yang ada

	'username' => 'root',
	'password' => '',
	'database' => 'db_rekrutmen_tenaga_ahli_hendra',

5. API sudah siap digunakan.
6. API ini dapat diakses tanpa menggunakan HTTP Request Headers
7. Untuk mengaksesnya , dapat diakses pada link URL {base_url}/puskesmas dengan metode GET
