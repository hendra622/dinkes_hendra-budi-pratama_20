<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Puskesmas extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Puskesmas_model');
    }

    public function index_get(){
        $puskesmas = $this->Puskesmas_model->list_puskesmas();
        $jumlah = count($puskesmas);
        $res = array();

        if ($jumlah > 0){
            foreach ($puskesmas as $p) {
                $data = array();
                $data['id_puskes'] = $p->id_puskesmas;
                $data['nama_puskesmas'] = $p->nama_puskesmas;
                $data['alamat_puskesmas'] = $p->alamat_puskesmas;
                $data['no_telp_puskesmas'] = $p->no_telp_puskesmas;
                $data['provinsi']['id_provinsi'] = $p->id_provinsi;
                $data['provinsi']['nama_provinsi'] = $p->nama_provinsi;
                $data['kota']['id_kota'] = $p->id_kota;
                $data['kota']['nama_kota'] = $p->nama_kota;
                $data['kecamatan']['id_kecamatan'] = $p->id_kecamatan;
                $data['kecamatan']['nama_kecamatan'] = $p->nama_kecamatan;

                $res[] = $data;
            }

            $this->set_response(
                array(
                    "status" => "success",
                    "code" => 200,
                    "count" => $jumlah,
                    "data" => $res
            ), REST_Controller::HTTP_OK); 
        } else {
            $this->set_response(
                array(
                    "status" => "Tidak ada data",
                    "kode" => 404,
                    "count" => 0,
                    "data" => array()
            ), REST_Controller::HTTP_NOT_FOUND); 
        }            
    }

}
